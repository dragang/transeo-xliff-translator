﻿using BusinessLayer;
using MvcFileUploader;
using MvcFileUploader.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace XliffTranslator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<string> filePaths = new List<string>();

            //Clears upload folder on load
            //DirectoryInfo di = new DirectoryInfo(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Content\\uploads")); // DELETE UPLOADS BEFORE PAGE LOAD
            //foreach (FileInfo file in di.GetFiles())
            //{
            //    file.Delete();
            //}
            //foreach (DirectoryInfo dir in di.GetDirectories())
            //{
            //    dir.Delete(true);
            //}
            string uploadFolder = Guid.NewGuid().ToString();
            ViewBag.guid = Guid.NewGuid().ToString();
            return View();
        }

        public ActionResult UploadFile(FormCollection formCollection) // optionally receive values specified with Html helper
        {
            Guid guid = Guid.Parse(formCollection["guidId"]);
            ViewBag.guid = guid;
            string fileFullPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Content\\uploads\\" + guid);
            
            if (!Directory.Exists(fileFullPath))
            {
                Directory.CreateDirectory(fileFullPath);
            }

            // here we can send in some extra info to be included with the delete url 
            var statuses = new List<ViewDataUploadFileResult>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                string extension = Path.GetExtension(Request.Files[i].FileName);
                // Checks for extension and if the content is smaller than 10MB
                if ((extension == ".xliff" || extension == ".xlf") && Request.Files[i].ContentLength < 10000000) {
                    var st = FileSaver.StoreFile(x =>
                    {
                        x.File = Request.Files[i];

                        // this is used to generate the relative url of the file
                        x.StorageDirectory = Server.MapPath("~/Content/uploads/" + guid);
                        x.UrlPrefix = "/Content/uploads/" + guid;


                        //overriding defaults
                        x.FileName = Request.Files[i].FileName;// default is filename suffixed with filetimestamp
                        x.ThrowExceptions = true;//default is false, if false exception message is set in error property
                    });
                    statuses.Add(st);
                }
                
            }

            statuses.ForEach(x => x.thumbnailUrl = x.url + "?width=80&height=80"); // uses ImageResizer httpmodule to resize images from this url

            //setting custom download url instead of direct url to file which is default
            statuses.ForEach(x => x.url = Url.Action("DownloadFile", new { fileUrl = x.url, mimetype = x.type }));

            var viewresult = Json(new { files = statuses });
            //for IE8 which does not accept application/json
            if (Request.Headers["Accept"] != null && !Request.Headers["Accept"].Contains("application/json"))
                viewresult.ContentType = "text/plain";

            return viewresult;
        }

        public ActionResult DownloadFile(string fileUrl, string mimetype)
        {
            var filePath = Server.MapPath("~" + fileUrl);

            if (System.IO.File.Exists(filePath))
                return File(filePath, mimetype);
            else
            {
                return new HttpNotFoundResult("File not found");
            }
        }

        public ActionResult DownloadExcel(string guid)
        {
            // Convertor from xliff to Excel
            try
            {
                List<string> filePaths = Directory.GetFiles(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Content\\uploads\\" + guid)).ToList();
                if (filePaths.Count != 0)
                {
                    byte[] excelBytes = new XliffHelper().GetXliffExcel(filePaths, HostingEnvironment.ApplicationPhysicalPath);
                    string newFileName = Guid.NewGuid().ToString();
                    string fileFullPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Output");
                    if (!Directory.Exists(fileFullPath))
                    {
                        Directory.CreateDirectory(fileFullPath);
                    }

                    ////Code for saving the excel file
                    //fileFullPath = Path.Combine(fileFullPath, Guid.NewGuid().ToString() + ".xlsx");
                    //System.IO.File.WriteAllBytes(fileFullPath, excelBytes);

                    ////Code for deleting upload folder after download
                    //DirectoryInfo di = new DirectoryInfo(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Content\\uploads\\" + guid));
                    //foreach (FileInfo file in di.GetFiles())
                    //{
                    //    file.Delete();
                    //}
                    //foreach (DirectoryInfo dir in di.GetDirectories())
                    //{
                    //    dir.Delete(true);
                    //}
                    string fileName = "ConvertedExcel.xlsx";
                    return File(excelBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
            }
            catch{}
            
            return RedirectToAction("Index");
        }
    }
}
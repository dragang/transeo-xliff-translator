﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class XliffResult
    {
        public string FileNumber { get; set; }
        public string Title { get; set; }
        public string Toast { get; set; }
        public string Body { get; set; }
        public string URL { get; set; }
        public string FullText { get; set; }
        public int Count { get; set; }
    }
}

﻿using Iborn.net.AIF.Spreadsheet.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace BusinessLayer
{
    public class XliffHelper
    {
        public byte[] GetXliffExcel(List<string> fileNames, string applicationPhysicalPath)
        {
            List<XliffResult> result = GetXliffResults(fileNames);
            string templatePath = Path.Combine(applicationPhysicalPath, "ExcelTemplates", "TemplateTranseo.xml");
            byte[] excelBytes = new ExcelExport().DownloadExcelFromList<XliffResult>(result, templatePath, "en", "Xliff Export");
            //byte[] excelBytes = new ExcelExport().DownloadExcelFromList<XliffResult>(result, "Xliff Export");
            return excelBytes;
        }
        public List<XliffResult> GetXliffResults(List<string> fileNames)
        {
            List<XliffResult> result = new List<XliffResult>();
            foreach (var fileName in fileNames)
            {
                result.Add(GetXliffResult(fileName));
            }
            int countSum = result.Sum(x => x.Count);
            result.Add(new XliffResult() { Body = "", Count = countSum, FileNumber = "", FullText = "", Title = "", Toast = "", URL = "" });
            return result;
        }

        public XliffResult GetXliffResult(string fileName)
        {
            var fileId = Regex.Replace(fileName, "(.xliff)|(.xlf)", "");
            fileId = Regex.Match(fileId, @"\d+$").Value;

            XliffResult result = new XliffResult();
            List<XElement> elements = new List<XElement>();
            XDocument doc1 = XDocument.Load(fileName);
            XNamespace df = doc1.Root.Name.Namespace;
            string TransUnitNodeValue = "";
            string regex = "(\\[vc_raw_html.*?\\].*?\\[/vc_raw_html\\])|(\\[.*?\\])|(\\<.*?\\>)";
            string spaceRegex = @"\s\s+";
            //string andRegex = "(&amp)";
            result.URL = doc1.Descendants(df + "external-file").Attributes("href").First().Value;
            foreach (XElement transUnitNode in doc1.Descendants(df + "trans-unit"))
            {
                string resname = transUnitNode.Attribute("resname").Value;
                XElement sourceNode = transUnitNode.Element(df + "source");
                TransUnitNodeValue = sourceNode.Value;

                if (resname == "title")
                {
                    result.Title = TransUnitNodeValue;
                }
                else if (resname == "field-_yoast_wpseo_title-0")
                {
                    result.Toast = TransUnitNodeValue;
                }
                else if (resname == "body")
                {
                    result.Body = Regex.Replace(TransUnitNodeValue, regex, "");
                    result.Body.Replace(spaceRegex, "");
                    //result.Body.Replace(andRegex, "&");
                }
            }
            result.FileNumber = fileId;
            result.FullText = result.Toast + " " + result.Title + " " + result.Body;
            result.Count = CountWords(result.FullText);
            return result;
        }

        private int CountWords(string text)
        {
            char[] delimiters = new char[] { ' ', '\r', '\n' };
            return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}

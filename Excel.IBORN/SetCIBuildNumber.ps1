$input_path = 'Excel.IBORN\AssemblyVersion.cs'
$regex = �\b[0-9]+\.[0-9]+(\.[0-9]+)?\b�
$version = select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches } | % { $_.Value }
Write-Host "##teamcity[buildNumber '$version']"
@echo off
setlocal EnableDelayedExpansion
set AssemblyVersionPath=AssemblyVersion.cs
IF "%1"=="" GOTO promptMode
:argumentMode
set version=%1
GOTO Change
:promptMode
for /f "tokens=*" %%a in (%AssemblyVersionPath%) do (
    set z=%%a
    set z=!z:"=_!
    for /f "tokens=2 delims=_" %%a in ("!z!") do (
		set assemblyVersion=%%a
	)
)
echo Assembly version is %assemblyVersion:~0,-1%
set /P question=Do you want to change the assembly version number [y/n]?
if not %question% == y GOTO Exit
set /P version=Enter the new version number:
:Change
echo [assembly: System.Reflection.AssemblyVersion("%version%.*")] > %AssemblyVersionPath%
echo Version changed to %version%.
:Exit
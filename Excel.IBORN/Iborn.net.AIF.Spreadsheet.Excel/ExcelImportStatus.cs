﻿using System.Collections.Generic;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelImportStatus
    {
        public bool valid { get; set; }
        public string status { get; set; }
        public string filename { get; set; }
        public int filesize { get; set; }
        public string error { get; set; }
        public List<ExcelError> errorList { get; set; }

        public ExcelImportStatus()
        {
            valid = true;
            errorList = new List<ExcelError>();
        }
    }
}

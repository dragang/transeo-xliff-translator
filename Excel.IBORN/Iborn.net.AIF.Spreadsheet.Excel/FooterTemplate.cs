﻿using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class FooterTemplate
    {
        public Formatting formatting { get; set; }

        public FooterTemplate(XElement XFooterTemplate, string Language = "en")
        {
            XElement formatting = XFooterTemplate.Descendants("formatting").FirstOrDefault();
            this.formatting = formatting == null ? null : new Formatting(formatting);
        }
    }
}

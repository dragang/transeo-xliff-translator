﻿using System.Collections.Generic;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelSheetSetup<T>
    {
        public List<T> listOfObjects { get; set; }
        public string templatePath { get; set; }
        public string sheetName { get; set; }
        public string language { get; set; }
        public bool printHeaders { get; set; }
        public bool printColumnNames { get; set; }
        public bool addFiltersOnColumnNames { get; set; }
        public bool applyFormatting { get; set; }
        public bool autoFitColumns { get; set; }
        public bool printFooters { get; set; }
        public string Notes { get; set; }

        public ExcelSheetSetup(List<T> listOfObjects, string templatePath, string sheetName, string language, bool printHeaders, bool printColumnNames, bool addFiltersOnColumnNames, bool applyFormatting, bool autoFitColumns, bool printFooters, string Notes = null)
        {
            this.listOfObjects = listOfObjects;
            this.templatePath = templatePath;
            this.sheetName = sheetName;
            this.language = language;
            this.printHeaders = printHeaders;
            this.printColumnNames = printColumnNames;
            this.addFiltersOnColumnNames = addFiltersOnColumnNames;
            this.applyFormatting = applyFormatting;
            this.autoFitColumns = autoFitColumns;
            this.printFooters = printFooters;
            this.Notes = Notes;
        }

        public ExcelSheetSetup(List<T> listOfObjects, string templatePath, string sheetName, string language, string Notes = null)
        {
            this.listOfObjects = listOfObjects;
            this.templatePath = templatePath;
            this.sheetName = sheetName;
            this.language = language;
            this.printHeaders = true;
            this.printColumnNames = true;
            this.addFiltersOnColumnNames = true;
            this.applyFormatting = true;
            this.autoFitColumns = true;
            this.printFooters = false;
            this.Notes = Notes;
        }
    }
}

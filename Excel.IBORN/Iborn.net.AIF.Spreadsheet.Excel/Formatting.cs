﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class Formatting
    {
        public string color { get; set; }
        public string fontcolor { get; set; }
        public string font { get; set; }
        public int? fontsize { get; set; }
        public string fontweight { get; set; }
        public string borders { get; set; }
        public string stringFormat { get; set; }
        public double? width { get; set; }
        public string image { get; set; }

        public Formatting(XElement XFormatting)
        {
            XElement color = XFormatting.Descendants("color").FirstOrDefault();
            this.color = color == null ? String.Empty : color.Value;

            XElement image = XFormatting.Descendants("image").FirstOrDefault();
            this.image = image == null ? String.Empty : image.Value;

            XElement fontcolor = XFormatting.Descendants("fontcolor").FirstOrDefault();
            this.fontcolor = fontcolor == null ? String.Empty : fontcolor.Value;

            XElement font = XFormatting.Descendants("font").FirstOrDefault();
            this.font = font == null ? String.Empty : font.Value;

            XElement fontsize = XFormatting.Descendants("fontsize").FirstOrDefault();
            int tryFontsize = 0;
            if (fontsize != null && Int32.TryParse(fontsize.Value, out tryFontsize))
                this.fontsize = tryFontsize;
            else
                this.fontsize = null;

            XElement fontweight = XFormatting.Descendants("fontweight").FirstOrDefault();
            this.fontweight = fontweight == null ? String.Empty : fontweight.Value;

            XElement borders = XFormatting.Descendants("border").FirstOrDefault();
            this.borders = borders == null ? String.Empty : borders.Value;

            XElement stringFormat = XFormatting.Descendants("stringFormat").FirstOrDefault();
            this.stringFormat = stringFormat == null ? String.Empty : stringFormat.Value;

            XElement width = XFormatting.Descendants("width").FirstOrDefault();
            double tryWidth = 0;
            if (width != null && double.TryParse(width.Value, out tryWidth))
                this.width = tryWidth;

        }

        public void ApplyFormattingToCells(ExcelRange cells)
        {
            if (!String.IsNullOrWhiteSpace(this.color))
            {
                cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cells.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(this.color));
            }

            if (!String.IsNullOrWhiteSpace(this.image))
            {
                string imagepath = this.image.ToString();
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(imagepath);
                var imageInFile = cells.Worksheet.Drawings.AddPicture("image", myImage);
                // Row, RowoffsetPixel, Column, ColumnOffSetPixel
                imageInFile.SetPosition(0, 0, 0, 0);
            }

            if (!String.IsNullOrWhiteSpace(this.fontcolor))
            {
                cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cells.Style.Font.Color.SetColor(ColorTranslator.FromHtml(this.fontcolor));
            }
            if (!String.IsNullOrWhiteSpace(this.borders))
            {
                Border border = cells.Style.Border;
                ExcelBorderStyle borderType = ExcelBorderStyle.None;
                System.Drawing.Color borderColor = System.Drawing.Color.Gray;

                if (this.borders.Equals("None", StringComparison.InvariantCultureIgnoreCase))
                {
                    border.BorderAround(ExcelBorderStyle.None, System.Drawing.Color.Gray);
                }
                if (this.borders.Equals("Thin", StringComparison.InvariantCultureIgnoreCase))
                {
                    borderType = ExcelBorderStyle.Thin;
                }
                if (this.borders.Equals("Thick", StringComparison.InvariantCultureIgnoreCase))
                {
                    borderType = ExcelBorderStyle.Thick;
                }
                if (this.borders.Equals("Dashed", StringComparison.InvariantCultureIgnoreCase))
                {
                    borderType = ExcelBorderStyle.Dashed;
                }
                if (this.borders.Equals("Double", StringComparison.InvariantCultureIgnoreCase))
                {
                    borderType = ExcelBorderStyle.Double;
                }
                border.BorderAround(borderType, System.Drawing.Color.Gray);
                border.Bottom.Style = borderType;
                border.Top.Style = borderType;
                border.Left.Style = borderType;
                border.Right.Style = borderType;
                border.Bottom.Color.SetColor(borderColor);
                border.Top.Color.SetColor(borderColor);
                border.Left.Color.SetColor(borderColor);
                border.Right.Color.SetColor(borderColor);
            }
            if (this.fontsize.HasValue)
            {
                cells.Style.Font.Size = this.fontsize.Value;
            }

            if (!String.IsNullOrWhiteSpace(this.font))
            {
                cells.Style.Font.Name = this.font;
            }
            if (!String.IsNullOrWhiteSpace(this.fontweight))
            {
                if (this.fontweight.Equals("bold", StringComparison.InvariantCultureIgnoreCase))
                {
                    cells.Style.Font.Bold = true;
                }
                else if (this.fontweight.Equals("normal", StringComparison.InvariantCultureIgnoreCase))
                {
                    cells.Style.Font.Bold = false;
                }
            }
            if (!String.IsNullOrWhiteSpace(this.stringFormat))
            {
                if (this.stringFormat.Contains("Date"))
                {
                    foreach (var cell in cells)
                    {
                        if (cell.Value != null && cell.Value.ToString() != "")
                        {
                            cell.Value = String.Format(this.stringFormat.Remove(0, 4), Convert.ToDateTime(cell.Value));
                        }
                    }
                }
                else
                {
                    foreach (var cell in cells)
                    {
                        cell.Value = String.Format(this.stringFormat, cell.Value);
                    }
                }
            }
            if (this.width.HasValue)
            {
                cells.Worksheet.Column(cells.Start.Column).Width = (this.width.Value / 7d) + 1;
            }
        }
    }
}

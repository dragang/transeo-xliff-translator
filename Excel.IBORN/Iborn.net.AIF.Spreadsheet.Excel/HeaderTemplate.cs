﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class HeaderTemplate
    {
        public string headerText { get; set; }
        public int? mergeGroup { get; set; }
        public Formatting formatting { get; set; }

        public HeaderTemplate(XElement XHeaderTemplate, string Language = "en")
        {
            XElement headerText = XHeaderTemplate.Descendants("headerText").Where(ht => ht.Attribute("language") != null && ht.Attribute("language").Value == Language).SingleOrDefault();
            this.headerText = headerText == null ? String.Empty : headerText.Value;

            XAttribute mergeGroup = XHeaderTemplate.Attribute("mergeGroup");
            int tryMergeGroup = 0;
            Int32.TryParse(mergeGroup.Value, out tryMergeGroup);
            this.mergeGroup = tryMergeGroup;

            XElement formatting = XHeaderTemplate.Descendants("formatting").FirstOrDefault();
            this.formatting = formatting == null ? null : new Formatting(formatting);
        }
    }
}

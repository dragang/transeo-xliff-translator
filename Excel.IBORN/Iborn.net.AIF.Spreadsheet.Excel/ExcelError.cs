﻿namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelError
    {
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public ExcelError() { }

        public ExcelError(int RowIndex, int ColumnIndex, string ColumnName, string ColumnValue, int ErrorCode, string ErrorMessage)
        {
            this.RowIndex = RowIndex;
            this.ColumnIndex = ColumnIndex;
            this.ColumnName = ColumnName;
            this.ColumnValue = ColumnValue;
            this.ErrorCode = ErrorCode;
            this.ErrorMessage = ErrorMessage;
        }
    }
}

﻿using System.Collections.Generic;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelSheetListOfObjects<T>
    {
        public List<T> ListOfObjects { get; set; }

        public ExcelSheetListOfObjects(List<T> listOfObjects)
        {
            this.ListOfObjects = listOfObjects;
        }
    }
}

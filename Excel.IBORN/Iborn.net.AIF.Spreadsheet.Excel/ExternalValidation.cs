﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExternalValidation
    {
        public string assemblyName { get; set; }
        public string className { get; set; }
        public string methodName { get; set; }
        public List<Arg> args { get; set; }

        public ExternalValidation(XElement XExternalValidation)
        {
            this.args = new List<Arg>();

            XElement assemblyName = XExternalValidation.Descendants("assemblyName").FirstOrDefault();
            this.assemblyName = assemblyName == null ? String.Empty : assemblyName.Value;

            XElement className = XExternalValidation.Descendants("className").FirstOrDefault();
            this.className = className == null ? String.Empty : className.Value;

            XElement methodName = XExternalValidation.Descendants("methodName").FirstOrDefault();
            this.methodName = methodName == null ? String.Empty : methodName.Value;

            XElement args = XExternalValidation.Descendants("args").FirstOrDefault();
            foreach (var arg in args.Descendants("arg"))
            {
                Arg a = new Arg(arg);
                this.args.Add(a);
            }
        }
    }
}

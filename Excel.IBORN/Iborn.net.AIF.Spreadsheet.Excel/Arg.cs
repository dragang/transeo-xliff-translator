﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class Arg
    {
        public string type { get; set; }
        public string binding { get; set; }
        public string value { get; set; }

        public Arg(XElement XArg)
        {
            XElement type = XArg.Descendants("type").FirstOrDefault();
            this.type = type == null ? String.Empty : type.Value;

            XElement binding = XArg.Descendants("binding").FirstOrDefault();
            this.binding = binding == null ? String.Empty : binding.Value;

            XElement value = XArg.Descendants("value").FirstOrDefault();
            this.value = value == null ? String.Empty : value.Value;
        }
    }
}

﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ColumnTemplate
    {
        public string excelColumn { get; set; }
        public string databaseColumn { get; set; }
        public bool nullable { get; set; }
        public string type { get; set; }
        public string validation { get; set; }
        public string compare { get; set; }
        public string compareOperation { get; set; }
        public string minValue { get; set; }
        public string maxValue { get; set; }
        public bool unique { get; set; }
        public string conditionalUnique { get; set; }
        public int mergeGroup { get; set; }
        public Formatting formatting { get; set; }
        public Formatting columnheaderformating { get; set; }
        public ExternalValidation externalValidation { get; set; }

        public ColumnTemplate(XElement XColumnTemplate, string Language = "en")
        {
            XElement excelColumn = XColumnTemplate.Descendants("excelColumn").Where(ec => ec.Attribute("language") != null && ec.Attribute("language").Value == Language).SingleOrDefault();
            this.excelColumn = excelColumn == null ? String.Empty : excelColumn.Value;

            XElement databaseColumn = XColumnTemplate.Descendants("databaseColumn").FirstOrDefault();
            this.databaseColumn = databaseColumn == null ? String.Empty : databaseColumn.Value;

            XElement nullable = XColumnTemplate.Descendants("nullable").FirstOrDefault();
            bool tryNullable = true;
            if (nullable != null)
                bool.TryParse(nullable.Value, out tryNullable);
            this.nullable = tryNullable;

            XElement type = XColumnTemplate.Descendants("type").FirstOrDefault();
            this.type = type == null ? String.Empty : type.Value;

            XElement validation = XColumnTemplate.Descendants("validation").FirstOrDefault();
            this.validation = validation == null ? String.Empty : validation.Value;

            XElement compare = XColumnTemplate.Descendants("compare").Where(ec => ec.Attribute("operation") != null).SingleOrDefault();
            this.compare = compare == null ? String.Empty : compare.Value;
            this.compareOperation = compare == null ? String.Empty : compare.Attribute("operation").Value;

            XElement minValue = XColumnTemplate.Descendants("minValue").FirstOrDefault();
            this.minValue = minValue == null ? String.Empty : minValue.Value;

            XElement maxValue = XColumnTemplate.Descendants("maxValue").FirstOrDefault();
            this.maxValue = maxValue == null ? String.Empty : maxValue.Value;

            XElement unique = XColumnTemplate.Descendants("unique").FirstOrDefault();
            bool tryUnique = false;
            if (unique != null)
                bool.TryParse(unique.Value, out tryUnique);
            this.unique = tryUnique;

            XElement conditionalUnique = XColumnTemplate.Descendants("conditionalUnique").FirstOrDefault();
            this.conditionalUnique = conditionalUnique == null ? String.Empty : conditionalUnique.Value;

            XAttribute mergeGroup = XColumnTemplate.Attribute("mergeGroup");
            int tryMergeGroup = 0;
            Int32.TryParse(mergeGroup.Value, out tryMergeGroup);
            this.mergeGroup = tryMergeGroup;

            XElement formatting = XColumnTemplate.Descendants("formatting").FirstOrDefault();
            this.formatting = formatting == null ? null : new Formatting(formatting);

            XElement externalValidation = XColumnTemplate.Descendants("externalValidation").FirstOrDefault();
            this.externalValidation = externalValidation == null ? null : new ExternalValidation(externalValidation);

            XElement columnheaderformating = XColumnTemplate.Descendants("columnHeaderformatting").FirstOrDefault();
            this.columnheaderformating = columnheaderformating == null ? null : new Formatting(columnheaderformating);
        }
    }
}

﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelImport
    {
        //Supported date formats
        private static string[] formats_da = { "d-M-yyyy", "d-MM-yyyy", "dd-M-yyyy", "dd-MM-yyyy", "d-M-yyyy h:mm:ss tt", "dd-MM-yyyy hh:mm:ss", "d-M-yyyy h:mm:ss", "d-M-yyyy hh:mm tt", "d-M-yyyy hh tt", "d-M-yyyy h:mm", "d-M-yyyy h:mm", "dd-MM-yyyy hh:mm", "dd-M-yyyy hh:mm" };

        //Supported date formats
        private static string[] formats_en = { "M/d/yyyy", "MM/d/yyyy", "M/d/yyyy", "MM/dd/yyyy", "M/d/yyyy h:mm:ss tt", "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", "M/d/yyyy h:mm", "M/d/yyyy h:mm", "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm" };

        public string[] formats(string culture)
        {
            if (culture == "da")
                return formats_da;
            if (culture == "en")
                return formats_en;
            else
                return null;
        }

        //Email validation regex
        private static Regex _emailRegex = new Regex(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        //Phone validation regex
        private static Regex _phoneRegex = new Regex(@"^(\+?\(?\+?[0-9]*\)?([0-9 \-\(\)]*))$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public enum ExcelErrorCodeEnum
        {
            MissingColumn = 1,
            InvalidColumnType = 2,
            InvalidCell = 3,
            DuplicateCell = 4
        }

        //Error strings
        public string ImportFailed { get; set; }
        public string MissingColumns { get; set; }
        public string InvalidColumnType { get; set; }
        public string InvalidCell { get; set; }
        public string DuplicateCell { get; set; }
        public string TrueString = "yes";
        public string FalseString = "no";

        public ExcelImport()
        {
            ImportFailed = "Import failed";
            MissingColumns = "There are missing columns";
            InvalidColumnType = "The type of the column {0} is not valid";
            InvalidCell = "Cell is not valid";
            DuplicateCell = "Cell must be unique";
        }

        public ExcelImport(string importFailed, string missingColumns, string invalidColumnType, string invalidCell, string duplicateCell)
        {
            ImportFailed = importFailed;
            MissingColumns = missingColumns;
            InvalidColumnType = invalidColumnType;
            InvalidCell = invalidCell;
            DuplicateCell = duplicateCell;
        }

        public ExcelImport(string importFailed, string missingColumns, string invalidColumnType, string invalidCell, string duplicateCell, string trueString, string falseString)
        {
            ImportFailed = importFailed;
            MissingColumns = missingColumns;
            InvalidColumnType = invalidColumnType;
            InvalidCell = invalidCell;
            DuplicateCell = duplicateCell;
            TrueString = trueString;
            FalseString = falseString;
        }

        /// <summary>
        /// Reads a given Excel file and returns it as a DataSet
        /// </summary>
        /// <param name="filePath">the file path of the excel file</param>
        /// <param name="addColumnNames">true if the first row after the skipped rows is the row with the column names (true by default)</param>
        /// <param name="skipTopRows">number of rows to skip from the top (0 by default)</param>
        /// <returns>a DataSet with the data from the Excel</returns>
        public DataSet ReadExcelFile(string filePath, bool addColumnNames = true, int skipTopRows = 0)
        {
            DataSet result = new DataSet();

            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                try
                {
                    IExcelDataReader excelReader = null;
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    excelReader.IsFirstRowAsColumnNames = false;
                    result = excelReader.AsDataSet();
                    excelReader.Close();
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("There is an error in the excel file. Please try with .xlsx format. Error: \"" + ex.Message + "\"");
                }
            }
            if (result == null || result.Tables.Count == 0)
            {
                try
                {
                    using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        IExcelDataReader excelReader2 = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        excelReader2.IsFirstRowAsColumnNames = false;
                        result = excelReader2.AsDataSet();
                        excelReader2.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("There is an error in the excel file. Please try with .xlsx format. Error: \"" + ex.Message + "\"");
                }
            }

            if (result == null || result.Tables.Count == 0)
            {
                string errorMsg = "This error usually comes up if there is something wrong with the file. Things you can try: Save the file as Excel 2007- type (.xlsx) or download the template again and copy the data into that one.";
                throw new InvalidOperationException(errorMsg);
            }

            //Delete the rows from 0 to skipTopRows - 1 (headers)
            if (skipTopRows > 0)
            {
                for (int i = 0; i < skipTopRows; i++)
                {
                    foreach (DataTable table in result.Tables)
                    {
                        table.Rows[i].Delete();
                        table.Rows[i].AcceptChanges();
                    }
                }
            }

            //add the names if the first row is with column names and delete the row
            if (addColumnNames)
            {
                foreach (DataTable table in result.Tables)
                {
                    if (table.Rows.Count == 0) continue;
                    foreach (DataColumn dc in table.Columns)
                    {
                        string colName = table.Rows[0][dc.Ordinal].ToString().Trim();
                        if (colName.EndsWith("*"))
                        {
                            colName = colName.Remove(colName.Length - 1).Trim();
                        }
                        if (!string.IsNullOrEmpty(colName) && !table.Columns.Contains(colName))
                        {
                            table.Columns[dc.Ordinal].ColumnName = colName.Trim();
                        }
                    }
                    table.Rows[0].Delete();
                    table.Rows[0].AcceptChanges();
                }
            }

            return result;
        }

        /// <summary>
        /// Validates the excel table using the template, parses and transfers the data in a new table with a schema corresponding to the database, and returns the new table
        /// </summary>
        /// <param name="excelTable">the table to be validated</param>
        /// <param name="template">the template used for validation and creating the new table</param>
        /// <param name="dataBaseTable">a new table with a schema corresponding to the database</param>
        /// <param name="rowIndexOffset">starting index of the first row with data</param>
        /// <returns>a bool indicating the validity of the table</returns>
        public ExcelImportStatus ValidateExcelAndCreateDataBaseTable(DataTable excelTable, ExcelTemplate template, out DataTable dataBaseTable, int rowIndexOffset = 0)
        {
            excelTable = TrimEmptyRows(excelTable);

            ExcelImportStatus retVal = new ExcelImportStatus();

            //create the new data table using the mappings
            dataBaseTable = new DataTable();
            foreach (ColumnTemplate columnMapping in template.columnMappings)
            {
                //Too complex...
                //dataBaseTable.Columns.Add(columnMapping.databaseColumn, Type.GetType("System.Collections.Generic.IEnumerable`1[System." + columnMapping.type + "]"));
                Type t = GetTypeByName(columnMapping.type);
                if (t != null)
                    dataBaseTable.Columns.Add(columnMapping.databaseColumn, t);
                else throw new TypeAccessException(string.Format(InvalidColumnType, columnMapping.databaseColumn));
            }

            //Validate that all not nullable columns are present in the excel 
            List<ExcelError> missingColumns = new List<ExcelError>();
            if (!ValidateExcelColumns(excelTable, template, out missingColumns))
            {
                retVal.error = MissingColumns;
                retVal.status = ImportFailed;
                retVal.valid = false;
                retVal.errorList = missingColumns;
                return retVal;
            }

            //Validate, parse and transfer the data into the new data table
            int rowIndex = 1 + rowIndexOffset;
            int realRowIndex = 0;
            foreach (DataRow row in excelTable.Rows)
            {
                DataRow dataBaseRow = dataBaseTable.NewRow();
                int columnIndex = 1;
                foreach (ColumnTemplate columnMapping in template.columnMappings)
                {
                    if (excelTable.Columns.Contains(columnMapping.excelColumn))
                    {
                        string input = row[columnMapping.excelColumn] == null ? null : row[columnMapping.excelColumn].ToString();
                        if (columnMapping.type == "String")
                        {
                            string parsedInput;
                            bool validInput = true;

                            if (!ValidateAndParseString(input, columnMapping.nullable, out parsedInput))
                                validInput = false;

                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.validation))
                            {
                                if (columnMapping.validation == "Email" && !ValidateAndParseEmail(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "Phone" && !ValidateAndParsePhone(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "Zip" && !ValidateAndParseZipCode(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "CPR" && !ValidateAndParseCPR(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "ShortCPR" && !ValidateAndParseShortCPR(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "CVR" && !ValidateAndParseCVR(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;

                                if (columnMapping.validation == "Pnumber" && !ValidateAndParsePnumber(input, columnMapping.nullable, out parsedInput))
                                    validInput = false;
                            }
                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.minValue) && !String.IsNullOrWhiteSpace(input))
                            {
                                int minLength = 0;
                                if (Int32.TryParse(columnMapping.minValue, out minLength))
                                {
                                    if (input.Length < minLength)
                                    {
                                        validInput = false;
                                    }
                                }
                            }
                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.maxValue) && !String.IsNullOrWhiteSpace(input))
                            {
                                int maxLength = 0;
                                if (Int32.TryParse(columnMapping.maxValue, out maxLength))
                                {
                                    if (input.Length > maxLength)
                                    {
                                        validInput = false;
                                    }
                                }
                            }

                            if (validInput && !String.IsNullOrWhiteSpace(parsedInput))
                                dataBaseRow[columnMapping.databaseColumn] = parsedInput;

                            if (!validInput)
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                columnIndex++;
                                continue;
                            }

                            //comparison validation
                            if (!String.IsNullOrWhiteSpace(columnMapping.compare) && !String.IsNullOrWhiteSpace(columnMapping.compareOperation))
                            {
                                //Get the name of the comparison column based on the excel name
                                var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == columnMapping.compare).FirstOrDefault();
                                if (dataBaseColumn == null)
                                    throw new NullReferenceException("The column " + columnMapping.compare + " stated as an argument in the template does not exist");
                                string input2 = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();

                                if (!CompareObjects(parsedInput, input2, columnMapping.compareOperation))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }

                        if (columnMapping.type == "Boolean")
                        {
                            bool? parsedInput;
                            bool validInput = true;

                            if (!ValidateAndParseBoolean(input, columnMapping.nullable, out parsedInput))
                                validInput = false;

                            if (validInput && parsedInput.HasValue)
                                dataBaseRow[columnMapping.databaseColumn] = parsedInput;

                            if (!validInput)
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                columnIndex++;
                                continue;
                            }

                            //comparison validation
                            if (!String.IsNullOrWhiteSpace(columnMapping.compare) && !String.IsNullOrWhiteSpace(columnMapping.compareOperation))
                            {
                                //Get the name of the comparison column based on the excel name
                                var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == columnMapping.compare).FirstOrDefault();
                                if (dataBaseColumn == null)
                                    throw new NullReferenceException("The column " + columnMapping.compare + " stated as an argument in the template does not exist");
                                string input2 = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();

                                bool? parsedCompareInput = null;
                                bool? parsedCompareInput1;
                                if (ValidateAndParseBoolean(input2, false, out parsedCompareInput1) && parsedCompareInput1.HasValue)
                                {
                                    parsedCompareInput = parsedCompareInput1.Value;
                                }

                                if (!CompareObjects(parsedInput, input2, columnMapping.compareOperation))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }

                        else if (columnMapping.type == "Integer")
                        {
                            int? parsedInput;
                            bool validInput = true;

                            if (!ValidateAndParseInt(input, columnMapping.nullable, out parsedInput))
                                validInput = false;

                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.minValue) && parsedInput.HasValue)
                            {
                                int minValue = 0;
                                if (Int32.TryParse(columnMapping.minValue, out minValue))
                                {
                                    if (parsedInput.Value < minValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }
                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.maxValue) && parsedInput.HasValue)
                            {
                                int maxValue = 0;
                                if (Int32.TryParse(columnMapping.maxValue, out maxValue))
                                {
                                    if (parsedInput.Value > maxValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }

                            if (validInput && parsedInput.HasValue)
                                dataBaseRow[columnMapping.databaseColumn] = parsedInput;

                            if (!validInput)
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                columnIndex++;
                                continue;
                            }

                            //comparison validation
                            if (parsedInput.HasValue && !String.IsNullOrWhiteSpace(columnMapping.compare) && !String.IsNullOrWhiteSpace(columnMapping.compareOperation))
                            {
                                //Get the name of the comparison column based on the excel name
                                var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == columnMapping.compare).FirstOrDefault();
                                if (dataBaseColumn == null)
                                    throw new NullReferenceException("The column " + columnMapping.compare + " stated as an argument in the template does not exist");
                                string input2 = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();

                                int? parsedCompareInput = null;
                                int? parsedCompareInput1;
                                if (ValidateAndParseInt(input2, false, out parsedCompareInput1) && parsedCompareInput1.HasValue)
                                {
                                    parsedCompareInput = parsedCompareInput1.Value;
                                }

                                if (parsedCompareInput.HasValue && !CompareObjects(parsedInput.Value, parsedCompareInput.Value, columnMapping.compareOperation))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }

                        else if (columnMapping.type == "Decimal")
                        {
                            decimal? parsedInput;
                            bool validInput = true;

                            if (!ValidateAndParseDecimal(input, columnMapping.nullable, out parsedInput))
                                validInput = false;

                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.minValue) && parsedInput.HasValue)
                            {
                                decimal minValue = 0;
                                if (decimal.TryParse(columnMapping.minValue, out minValue))
                                {
                                    if (parsedInput.Value < minValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }
                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.maxValue) && parsedInput.HasValue)
                            {
                                decimal maxValue = 0;
                                if (decimal.TryParse(columnMapping.maxValue, out maxValue))
                                {
                                    if (parsedInput.Value > maxValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }

                            if (validInput && parsedInput.HasValue)
                                dataBaseRow[columnMapping.databaseColumn] = parsedInput;

                            if (!validInput)
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                columnIndex++;
                                continue;
                            }

                            //comparison validation
                            if (parsedInput.HasValue && !String.IsNullOrWhiteSpace(columnMapping.compare) && !String.IsNullOrWhiteSpace(columnMapping.compareOperation))
                            {
                                //Get the name of the comparison column based on the excel name
                                var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == columnMapping.compare).FirstOrDefault();
                                if (dataBaseColumn == null)
                                    throw new NullReferenceException("The column " + columnMapping.compare + " stated as an argument in the template does not exist");
                                string input2 = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();

                                decimal? parsedCompareInput = null;
                                decimal? parsedCompareInput1;
                                if (ValidateAndParseDecimal(input2, false, out parsedCompareInput1) && parsedCompareInput1.HasValue)
                                {
                                    parsedCompareInput = parsedCompareInput1.Value;
                                }

                                if (parsedCompareInput.HasValue && !CompareObjects(parsedInput.Value, parsedCompareInput.Value, columnMapping.compareOperation))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }

                        else if (columnMapping.type == "Date")
                        {
                            DateTime? parsedInput = null;
                            DateTime? parsedInput1;
                            DateTime? parsedInput2;
                            bool validInput = true;

                            bool validDate1 = false;
                            bool validDate2 = false;

                            if (ValidateAndParseDateFromDouble(input, columnMapping.nullable, out parsedInput1))
                            {
                                validDate1 = true;
                                parsedInput = parsedInput1;
                            }

                            if (ValidateAndParseDate(input, columnMapping.nullable, false, formats(template.Language), out parsedInput2))
                            {
                                validDate2 = true;
                                parsedInput = parsedInput2;
                            }

                            if (!validDate1 && !validDate2)
                                validInput = false;

                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.validation))
                            {
                                if (columnMapping.validation == "LastDayOfMonth" && parsedInput.HasValue && !ValidateLastDayOfMonth(parsedInput.Value))
                                    validInput = false;
                            }


                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.minValue) && parsedInput.HasValue)
                            {
                                DateTime minValue = DateTime.MinValue;
                                if (DateTime.TryParseExact(columnMapping.minValue, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
                                {
                                    if (parsedInput.Value < minValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }
                            if (validInput && !String.IsNullOrWhiteSpace(columnMapping.maxValue) && parsedInput.HasValue)
                            {
                                DateTime maxValue = DateTime.MinValue;
                                if (DateTime.TryParseExact(columnMapping.maxValue, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out maxValue))
                                {
                                    if (parsedInput.Value > maxValue)
                                    {
                                        validInput = false;
                                    }
                                }
                            }

                            if (validInput && parsedInput.HasValue)
                                dataBaseRow[columnMapping.databaseColumn] = parsedInput;

                            if (!validInput)
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                columnIndex++;
                                continue;
                            }

                            //comparison validation
                            if (parsedInput.HasValue && !String.IsNullOrWhiteSpace(columnMapping.compare) && !String.IsNullOrWhiteSpace(columnMapping.compareOperation))
                            {
                                //Get the name of the comparison column based on the excel name
                                var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == columnMapping.compare).FirstOrDefault();
                                if (dataBaseColumn == null)
                                    throw new NullReferenceException("The column " + columnMapping.compare + " stated as an argument in the template does not exist");
                                string input2 = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();

                                DateTime? parsedCompareInput = null;
                                DateTime? parsedCompareInput1;
                                DateTime? parsedCompareInput2;

                                if (ValidateAndParseDateFromDouble(input2, false, out parsedCompareInput1))
                                {
                                    parsedCompareInput = parsedCompareInput1;
                                }

                                if (ValidateAndParseDate(input2, false, false, formats(template.Language), out parsedCompareInput2))
                                {
                                    parsedCompareInput = parsedCompareInput2;
                                }

                                if (parsedCompareInput.HasValue && !CompareObjects(parsedInput.Value, parsedCompareInput.Value, columnMapping.compareOperation))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.InvalidCell, InvalidCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }
                        //check if the column has conditional unique validation
                        if (columnMapping.conditionalUnique != null && !String.IsNullOrWhiteSpace(columnMapping.conditionalUnique))
                        {
                            bool conditionalUniquePass = true;
                            ColumnTemplate conditionalMapping = template.columnMappings.Where(x => x.databaseColumn == columnMapping.conditionalUnique).SingleOrDefault();
                            string conditionalInput = row[conditionalMapping.excelColumn] == null ? null : row[conditionalMapping.excelColumn].ToString();
                            if (!IsUnique(conditionalMapping, conditionalInput, realRowIndex, excelTable))
                            {
                                conditionalUniquePass = false;
                            }
                            if (conditionalMapping.externalValidation != null)
                            {
                                if (!ExternalValidation(conditionalMapping, template, row))
                                {
                                    conditionalUniquePass = false;
                                }
                            }
                            if (!conditionalUniquePass) //if conditional unique validation failed check this column for uniqueness
                            {
                                if (!IsUnique(columnMapping, input, realRowIndex, excelTable))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.DuplicateCell, DuplicateCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                }
                                if (!ExternalValidation(columnMapping, template, row))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.DuplicateCell, DuplicateCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                    break;
                                }
                            }
                        }
                        //check if cell value is unique
                        if (columnMapping.unique == true && String.IsNullOrWhiteSpace(columnMapping.conditionalUnique))
                        {
                            if (columnMapping.databaseColumn == "CPR" && input.Length >= 10)
                            {
                                if (!IsUnique(columnMapping, input.Replace("-", ""), realRowIndex, excelTable))
                                {
                                    ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.DuplicateCell, DuplicateCell);
                                    retVal.errorList.Add(e);
                                    retVal.valid = false;
                                    retVal.status = ImportFailed;
                                }
                            }
                            if (columnMapping.databaseColumn != "CPR" && !IsUnique(columnMapping, input, realRowIndex, excelTable))
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.DuplicateCell, DuplicateCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                            }
                        }
                        //external validation
                        if (columnMapping.externalValidation != null && String.IsNullOrWhiteSpace(columnMapping.conditionalUnique))
                        {
                            if (!ExternalValidation(columnMapping, template, row))
                            {
                                ExcelError e = new ExcelError(rowIndex, columnIndex, columnMapping.excelColumn, input, (int)ExcelErrorCodeEnum.DuplicateCell, DuplicateCell);
                                retVal.errorList.Add(e);
                                retVal.valid = false;
                                retVal.status = ImportFailed;
                                break;
                            }
                        }
                    }
                    columnIndex++;
                }
                dataBaseTable.Rows.Add(dataBaseRow);
                realRowIndex++;
                rowIndex++;
            }
            return retVal;
        }

        /// <summary>
        /// Checks if all not nullable columns in the template are present in the table
        /// </summary>
        /// <param name="table">the table to be checked</param>
        /// <param name="template">the template used for checking</param>
        /// <param name="missingColumns">if a column is missing it will be returned in this list</param>
        /// <returns>a bool indicating the validity of the table</returns>
        private bool ValidateExcelColumns(DataTable table, ExcelTemplate template, out List<ExcelError> missingColumns)
        {
            bool retVal = true;
            missingColumns = new List<ExcelError>();
            //Check if all not nullable columns are present in the excel
            int columnIndex = 1;
            foreach (ColumnTemplate columnMapping in template.columnMappings)
            {
                if (columnMapping.nullable == false && !table.Columns.Contains(columnMapping.excelColumn))
                {
                    retVal = false;
                    ExcelError e = new ExcelError();
                    e.ColumnName = columnMapping.excelColumn;
                    e.ColumnIndex = columnIndex;
                    e.ErrorCode = (int)ExcelErrorCodeEnum.MissingColumn;
                    missingColumns.Add(e);
                }
                columnIndex++;
            }
            return retVal;
        }

        /// <summary>
        /// Validates and parses a given string
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseString(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input) && !(input.Contains("\n") || input.Contains("\t")))
            {
                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given bool
        /// </summary>
        /// <param name="input">the bool to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseBoolean(string input, bool isNullable, out bool? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            retVal = false;
            bool result = true;
            if (TrueString.ToLower().Equals(input.ToLower()))
                retVal = true;
            else if (FalseString.ToLower().Equals(input.ToLower()))
                retVal = false;
            else
                result = false;
            return result;
        }

        /// <summary>
        /// Validates and parses a given integer
        /// </summary>
        /// <param name="input">the integer to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseInt(string input, bool isNullable, out int? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            int parsedValue = 0;
            bool result = int.TryParse(input, out parsedValue);
            retVal = parsedValue;
            return result;
        }

        /// <summary>
        /// Validates and parses a given OLE Automation date 
        /// </summary>
        /// <param name="input">the double OLE Automation representation of the date to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseDateFromDouble(string input, bool isNullable, out DateTime? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            retVal = null;

            double d;
            DateTime parsedValue = DateTime.MinValue;
            bool result = double.TryParse(input, out d);
            if (result)
                parsedValue = DateTime.FromOADate(d);
            retVal = parsedValue;
            return result;
        }

        /// <summary>
        /// Validates and parses a given date
        /// </summary>
        /// <param name="input">the date to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="containsHashes">a bool indicating whether the input date starts and ends with #</param>
        /// <param name="format">an array of formats (the date is valid if it satisfies at least one format)</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseDate(string input, bool isNullable, bool containsHashes, string[] format, out DateTime? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            retVal = null;
            //we must check if there is a hash key "#" in front and behind the string.
            if (containsHashes && !string.IsNullOrEmpty(input) && !(input.StartsWith("#") && input.EndsWith("#")))
                return false;

            DateTime parsedValue = DateTime.MinValue;
            bool result = DateTime.TryParseExact(input.Replace("#", ""), format, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedValue);
            retVal = parsedValue;
            return result;
        }

        /// <summary>
        /// Validates and parses a given date
        /// </summary>
        /// <param name="input">the date to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="containsHashes">a bool indicating whether the input date starts and ends with #</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseDate(string input, bool isNullable, bool containsHashes, out DateTime? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            retVal = null;
            //we must check if there is a hash key "#" in front and behind the string.
            if (containsHashes && !string.IsNullOrEmpty(input) && !(input.StartsWith("#") && input.EndsWith("#")))
                return false;

            string format = "yyyy-MM-dd";

            DateTime parsedValue = DateTime.MinValue;
            bool result = DateTime.TryParseExact(input.Replace("#", ""), format, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedValue);
            retVal = parsedValue;
            return result;
        }

        /// <summary>
        /// Validates and parses a given decimal
        /// </summary>
        /// <param name="input">the decimal to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseDecimal(string input, bool isNullable, out decimal? retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            //This accepts only numbers with optional decimal separator and two decimal digits
            Regex re = new Regex(@"^[1-9][0-9]*(.[0-9]{1,2})?$|^0+(.[0-9]{1,2})?$");
            MatchCollection mc = re.Matches(input);
            if (mc.Count == 0)
            {
                retVal = 0;
                return false;
            }
            decimal parsedValue = 0;
            System.IFormatProvider format = new System.Globalization.CultureInfo("en-US", true);
            bool result = decimal.TryParse(input, NumberStyles.Number, format, out parsedValue);
            retVal = parsedValue;
            return result;
        }

        /// <summary>
        /// Validates and parses a given email address
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseEmail(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input) && _emailRegex.Match(input).Length > 0)
            {
                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given phone number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParsePhone(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input) && _phoneRegex.Match(input).Length > 0)
            {
                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given CPR number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseCPR(string input, bool isNullable, out string retVal)
        {
            return ValidateAndParseCPRImplementation(input, isNullable, false, out retVal);
        }

        /// <summary>
        /// Validates and parses a given CPR number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseShortCPR(string input, bool isNullable, out string retVal)
        {
            return ValidateAndParseCPRImplementation(input, isNullable, true, out retVal);
        }

        /// <summary>
        /// Validates and parses a given CPR number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseCPRImplementation(string input, bool isNullable, bool allowShortCPR, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input))
            {
                input = input.Replace("-", "");

                if (!allowShortCPR && input.Length != 10)
                {
                    retVal = null;
                    return false;
                }

                if (allowShortCPR && input.Length != 10 && input.Length != 6)
                {
                    retVal = null;
                    return false;
                }

                DateTime? tryDate = null;
                try
                {
                    int day = int.Parse(input.Substring(0, 2));
                    int month = int.Parse(input.Substring(2, 2));
                    int year = int.Parse(input.Substring(4, 2));
                    year = (year > (DateTime.Now.Year - 2000)) ? year + 1900 : year + 2000;
                    tryDate = new DateTime(year, month, day);
                }
                catch
                {
                    retVal = null;
                    return false;
                }

                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given CVR number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseCVR(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input))
            {
                input = input.Replace("-", "").Trim();

                if (input.Length != 8)
                {
                    retVal = null;
                    return false;
                }

                long tryLong = 0;
                if (!long.TryParse(input, out tryLong))
                {
                    retVal = null;
                    return false;
                }

                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given P number
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParsePnumber(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input))
            {
                input = input.Replace("-", "").Trim();

                if (input.Length > 10)
                {
                    retVal = null;
                    return false;
                }

                long tryLong = 0;
                if (!long.TryParse(input, out tryLong))
                {
                    retVal = null;
                    return false;
                }

                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates and parses a given zip code
        /// </summary>
        /// <param name="input">the string to be validated</param>
        /// <param name="isNullable">a bool indicating whether the input can be null</param>
        /// <param name="retVal">the parsed value</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateAndParseZipCode(string input, bool isNullable, out string retVal)
        {
            if (isNullable && string.IsNullOrEmpty(input))
            {
                retVal = null;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(input))
            {
                input = input.Trim();

                if (input.Length != 4)
                {
                    retVal = null;
                    return false;
                }

                int tryInt = 0;
                if (!Int32.TryParse(input, out tryInt))
                {
                    retVal = null;
                    return false;
                }

                retVal = input;
                return true;
            }
            else
            {
                retVal = null;
                return false;
            }
        }

        /// <summary>
        /// Validates a given Date as end of month
        /// </summary>
        /// <param name="input">the date to be validated</param>
        /// <returns>a bool indicating whether the input is valid</returns>
        private bool ValidateLastDayOfMonth(DateTime input)
        {
            DateTime endOfMonth = new DateTime(input.Year, input.Month, DateTime.DaysInMonth(input.Year, input.Month));
            if (input != endOfMonth)
                return false;
            return true;
        }

        /// <summary>
        /// Compares 2 objects
        /// </summary>
        /// <param name="arg1">First object</param>
        /// <param name="arg2">Second object</param>
        /// <param name="type">Type of the objects (they must be the same)</param>
        /// <param name="operation">Comparison operation (==, !=, >=, ...)</param>
        /// <returns>a bool indicating the comparison value</returns>
        private bool CompareObjects(object arg1, object arg2, string operation)
        {
            IComparable obj1 = (IComparable)arg1;
            IComparable obj2 = (IComparable)arg2;

            if (obj1 == null || obj2 == null)
            {
                return false;
            }

            int result = obj1.CompareTo(obj2);
            if (operation == "==")
            {
                return result == 0;
            }
            else if (operation == "!=")
            {
                return result != 0;
            }
            else if (operation == ">")
            {
                return result == 1;
            }
            else if (operation == "<")
            {
                return result == -1;
            }
            else if (operation == ">=")
            {
                return result >= 0;
            }
            else if (operation == "<=")
            {
                return result <= 0;
            }
            return false;
        }

        /// <summary>
        /// Checks if a column is unique in the excel
        /// </summary>
        /// <param name="columnMapping">the column mapping</param>
        /// <param name="input">the column value</param>
        /// <param name="rowIndex">index of the row (starting position of the check)</param>
        /// <param name="excelTable">the table containing the column to be checked</param>
        /// <returns>true if the column is unique or empty</returns>
        private bool IsUnique(ColumnTemplate columnMapping, string input, int rowIndex, DataTable excelTable)
        {
            if (!(columnMapping.nullable && String.IsNullOrWhiteSpace(input)))
            {
                for (int i = 0; i < rowIndex; i++)
                {
                    string compareInput = excelTable.Rows[i][columnMapping.excelColumn] == null ? null : excelTable.Rows[i][columnMapping.excelColumn].ToString();
                    if (compareInput.Equals(input, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Executes external validation for a given column
        /// </summary>
        /// <param name="columnMapping">the column mapping</param>
        /// <param name="template">the excel template</param>
        /// <param name="row">the row containing the data</param>
        /// <returns>the result from the external validation</returns>
        private bool ExternalValidation(ColumnTemplate columnMapping, ExcelTemplate template, DataRow row)
        {
            string assemblyName = columnMapping.externalValidation.assemblyName;
            string className = columnMapping.externalValidation.className;
            string methodName = columnMapping.externalValidation.methodName;

            Object[] args = new Object[columnMapping.externalValidation.args.Count];
            for (int i = 0; i < columnMapping.externalValidation.args.Count; i++)
            {
                Arg a = columnMapping.externalValidation.args[i];
                if (a.binding == "Column")
                {
                    //Get the name of the column based on the excel name
                    var dataBaseColumn = template.columnMappings.Where(x => x.databaseColumn == a.value).FirstOrDefault();
                    if (dataBaseColumn == null)
                        throw new NullReferenceException("The column " + a.value + " stated as an argument in the template does not exist");
                    args[i] = row[dataBaseColumn.excelColumn] == null ? null : row[dataBaseColumn.excelColumn].ToString();
                }
                else if (a.binding == "Static")
                {
                    if (a.type == "String")
                        args[i] = a.value.ToString();
                    else if (a.type == "Integer")
                        args[i] = Int32.Parse(a.value.ToString());
                    else if (a.type == "Decimal")
                        args[i] = Decimal.Parse(a.value.ToString());
                    else if (a.type == "Date")
                        args[i] = DateTime.Parse(a.value.ToString());
                }
            }

            Object Result = DynaInvoke.InvokeMethod(assemblyName, className, methodName, args);
            bool boolResult = (bool)Result;
            return boolResult;
        }

        /// <summary>
        /// Get a type by name
        /// </summary>
        /// <param name="typeName">The name of the type (can be string, integer, decimal or a date)</param>
        /// <returns>the type for the given name</returns>
        private Type GetTypeByName(string typeName)
        {
            if (typeName.Equals("string", StringComparison.InvariantCultureIgnoreCase))
                return typeof(string);
            if (typeName.Equals("integer", StringComparison.InvariantCultureIgnoreCase) || typeName.Equals("int", StringComparison.InvariantCultureIgnoreCase) || typeName.Equals("int32", StringComparison.InvariantCultureIgnoreCase))
                return typeof(int);
            if (typeName.Equals("boolean", StringComparison.InvariantCultureIgnoreCase) || typeName.Equals("bool", StringComparison.InvariantCultureIgnoreCase) || typeName.Equals("Bool", StringComparison.InvariantCultureIgnoreCase))
                return typeof(bool);
            if (typeName.Equals("decimal", StringComparison.InvariantCultureIgnoreCase))
                return typeof(decimal);
            if (typeName.Equals("date", StringComparison.InvariantCultureIgnoreCase) || typeName.Equals("datetime", StringComparison.InvariantCultureIgnoreCase))
                return typeof(DateTime);
            else
                return null;
        }

        /// <summary>
        /// Removes the empty rows from the end of the data table
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private DataTable TrimEmptyRows(DataTable table)
        {
            bool isEmpty;
            for (int i = table.Rows.Count - 1; i >= 0; i--)
            {
                isEmpty = true;
                var tableRowItems = table.Rows[i].ItemArray;
                foreach (var item in tableRowItems)
                {
                    if (item != null && !string.IsNullOrEmpty(item.ToString().Trim()))
                    {
                        isEmpty = false;
                        break;
                    }
                }
                if (isEmpty)
                {
                    table.Rows[i].Delete();
                    table.Rows[i].AcceptChanges();
                }
                else
                {
                    break;
                }
            }
            return table;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelTemplate
    {
        public List<HeaderTemplate> headerMappings { get; set; }
        public List<ColumnTemplate> columnMappings { get; set; }
        public List<FooterTemplate> footerMappings { get; set; }
        public string Language;
        public string Name { get; set; }
        public string Notes { get; set; }

        public ExcelTemplate(string templatePath, string Language = "en", string Notes = null)
        {
            this.headerMappings = new List<HeaderTemplate>();
            this.columnMappings = new List<ColumnTemplate>();
            this.footerMappings = new List<FooterTemplate>();
            this.Language = Language;
            this.Name = string.Empty;
            this.Notes = Notes;

            XDocument doc = XDocument.Load(templatePath);
            XElement HeaderMappings = doc.Descendants("headerMappings").FirstOrDefault();
            XElement ColumnMappings = doc.Descendants("columnMappings").FirstOrDefault();
            XElement FooterMappings = doc.Descendants("footerMappings").FirstOrDefault();

            if (HeaderMappings != null)
            {
                foreach (var headerMapping in HeaderMappings.Descendants("headerTemplate"))
                {
                    HeaderTemplate ht = new HeaderTemplate(headerMapping, Language);
                    this.headerMappings.Add(ht);
                }
            }

            if (ColumnMappings != null)
            {
                foreach (var columnMapping in ColumnMappings.Descendants("columnTemplate"))
                {
                    ColumnTemplate ct = new ColumnTemplate(columnMapping, Language);
                    this.columnMappings.Add(ct);
                }
            }

            if (FooterMappings != null)
            {
                foreach (var footerMapping in FooterMappings.Descendants("footerTemplate"))
                {
                    FooterTemplate ft = new FooterTemplate(footerMapping, Language);
                    this.footerMappings.Add(ft);
                }
            }

        }

        public ExcelTemplate(XElement template, string Language = "en", string Notes = null)
        {
            this.headerMappings = new List<HeaderTemplate>();
            this.columnMappings = new List<ColumnTemplate>();
            this.footerMappings = new List<FooterTemplate>();
            this.Language = Language;
            this.Name = string.Empty;
            this.Notes = Notes;

            XAttribute name = template.Attribute("name");
            this.Name = name.Value;

            XElement HeaderMappings = template.Descendants("headerMappings").FirstOrDefault();
            XElement ColumnMappings = template.Descendants("columnMappings").FirstOrDefault();
            XElement FooterMappings = template.Descendants("footerMappings").FirstOrDefault();

            if (HeaderMappings != null)
            {
                foreach (var headerMapping in HeaderMappings.Descendants("headerTemplate"))
                {
                    HeaderTemplate ht = new HeaderTemplate(headerMapping, Language);
                    this.headerMappings.Add(ht);
                }
            }

            if (ColumnMappings != null)
            {
                foreach (var columnMapping in ColumnMappings.Descendants("columnTemplate"))
                {
                    ColumnTemplate ct = new ColumnTemplate(columnMapping, Language);
                    this.columnMappings.Add(ct);
                }
            }

            if (FooterMappings != null)
            {
                foreach (var footerMapping in FooterMappings.Descendants("footerTemplate"))
                {
                    FooterTemplate ft = new FooterTemplate(footerMapping, Language);
                    this.footerMappings.Add(ft);
                }
            }
        }
    }
}

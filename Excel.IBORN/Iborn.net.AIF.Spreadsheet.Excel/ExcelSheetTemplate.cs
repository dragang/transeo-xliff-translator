﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelSheetTemplate
    {
        public List<ExcelTemplate> sheets { get; set; }

        public ExcelSheetTemplate(string templatePath, string Language = "en")
        {
            XDocument doc = XDocument.Load(templatePath);
            this.sheets = new List<ExcelTemplate>();

            foreach (var sheet in doc.Descendants("sheet"))
            {
                ExcelTemplate et = new ExcelTemplate(sheet, Language);
                this.sheets.Add(et);
            }
        }
    }
}

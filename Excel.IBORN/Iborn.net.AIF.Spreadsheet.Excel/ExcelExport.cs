﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Iborn.net.AIF.Spreadsheet.Excel
{
    public class ExcelExport
    {
        public string TrueString = "yes";
        public string FalseString = "no";

        public ExcelExport()
        {
            TrueString = "yes";
            FalseString = "no";
        }

        public ExcelExport(string trueString, string falseString)
        {
            TrueString = trueString;
            FalseString = falseString;
        }

        /// <summary>
        /// Creates an Excel file for a given template
        /// </summary>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <returns>Excel file as byte array</returns>
        public byte[] DownloadExcelTemplate(string templatePath, string language, string sheetName, bool addAsteriskOnRequiredColumns = false, string Notes = null)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelTemplate excelTemplate = new ExcelTemplate(templatePath, language, Notes);

                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                CreateExcelDocument(excelTemplate, ws, addAsteriskOnRequiredColumns);

                //return the file as byte array
                return pck.GetAsByteArray();
            }
        }

        /// <summary>
        /// Creates an Excel file for a given template. This function is used for templates with more than one worksheet
        /// </summary>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <returns></returns>
        public byte[] DownloadExcelTemplate(string templatePath, string language, bool addAsteriskOnRequiredColumns = false)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelSheetTemplate sheetsTemplate = new ExcelSheetTemplate(templatePath, language);

                foreach (var excelTemplate in sheetsTemplate.sheets)
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(excelTemplate.Name);

                    //Create the document; 
                    CreateExcelDocument(excelTemplate, ws, addAsteriskOnRequiredColumns);
                }

                //return the file as byte array
                return pck.GetAsByteArray();
            }
        }

        public ExcelWorksheet CreateExcelDocument(ExcelTemplate excelTemplate, ExcelWorksheet ws, bool addAsteriskOnRequiredColumns)
        {
            int j = 1;

            int currentRowIndex = 1;

            //ADD NOTES
            if (!string.IsNullOrEmpty(excelTemplate.Notes))
            {
                //merge for all columns
                int numMergedColls = excelTemplate.columnMappings.Count;
                ws.Cells[currentRowIndex, j, currentRowIndex, j + numMergedColls - 1].Merge = true;
                ws.Cells[currentRowIndex, j, currentRowIndex, j + numMergedColls - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                ws.Cells[currentRowIndex, j].Value = excelTemplate.Notes;

                currentRowIndex++;
            }

            //Add headers
            for (int i = 0; i < excelTemplate.headerMappings.Count; i++)
            {
                ws.Cells[currentRowIndex, j].Value = excelTemplate.headerMappings[i].headerText;

                //get the number of columns in this group
                int numMergedColls = excelTemplate.columnMappings.Where(cm => cm.mergeGroup == excelTemplate.headerMappings[i].mergeGroup).Count();
                if (numMergedColls == 0)
                    numMergedColls = 1;

                if (numMergedColls > 1)
                {
                    ws.Cells[currentRowIndex, j, currentRowIndex, j + numMergedColls - 1].Merge = true;
                    ws.Cells[currentRowIndex, j, currentRowIndex, j + numMergedColls - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }

                if (excelTemplate.headerMappings[i].formatting != null)
                {
                    if (numMergedColls > 1)
                    {
                        excelTemplate.headerMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRowIndex, j, 1, j + numMergedColls - 1]);
                    }
                    else
                    {
                        excelTemplate.headerMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRowIndex, j]);
                    }
                }

                j += numMergedColls;
            }

            if (excelTemplate.headerMappings.Count > 0)
                currentRowIndex++;

            //add column names
            for (int i = 0; i < excelTemplate.columnMappings.Count(); i++)
            {
                if (addAsteriskOnRequiredColumns == true && excelTemplate.columnMappings[i].nullable == false)
                {
                    ws.Cells[currentRowIndex, i + 1].Value = excelTemplate.columnMappings[i].excelColumn + " *";
                }
                else
                {
                    ws.Cells[currentRowIndex, i + 1].Value = excelTemplate.columnMappings[i].excelColumn;
                }

                if (excelTemplate.columnMappings[i].formatting != null)
                {
                    excelTemplate.columnMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRowIndex, i + 1]);
                }
            }

            if (excelTemplate.columnMappings.Count > 0)
                currentRowIndex++;

            return ws;
        }

        /// <summary>
        /// Creates an Excel file for and fills it with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <param name="printHeaders">print the headers in the first row from the property names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        /// <returns>Excel file as byte array</returns>
        public byte[] DownloadExcelFromList<T>(List<T> listOfObjects, string sheetName, bool printHeaders = true, bool autoFitColumns = true)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //Populate the data
                if (listOfObjects.Count() > 0)
                {
                    Type objType = typeof(T);
                    MemberInfo[] members = objType.FindMembers(MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null);

                    if (printHeaders)
                    {
                        int col = 1;
                        foreach (var t in members)
                        {
                            ws.Cells[1, col++].Value = t.Name.Replace('_', ' ');
                        }
                    }

                    LoadExcelFromCollection<T>(listOfObjects, ws, 2, 1, members);

                    //set auto column width
                    if (autoFitColumns)
                    {
                        ws.Cells.AutoFitColumns();
                    }

                }

                return pck.GetAsByteArray();
            }
        }

        /// <summary>
        /// Creates an Excel file for a given template and fills it with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <param name="printHeaders">print the headers in the first row and merge the header groups</param>
        /// <param name="printColumnNames">print the column names above the data</param>
        /// <param name="addFiltersOnColumnNames">add filters for the columns on the column names</param>
        /// <param name="applyFormatting">apply the template formatting to the headers and column names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        /// <returns>Excel file as byte array</returns>
        public byte[] DownloadExcelFromList<T>(List<T> listOfObjects, string templatePath, string language, string sheetName, bool printHeaders = true, bool printColumnNames = true, bool addFiltersOnColumnNames = true, bool applyFormatting = true, bool autoFitColumns = true, bool addAsteriskOnRequiredColumns = false, bool printFooters = false, string Notes = null)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                FillExcelSheetWithData(pck, listOfObjects, templatePath, language, sheetName, printHeaders, printColumnNames, addFiltersOnColumnNames, applyFormatting, autoFitColumns, printFooters, addAsteriskOnRequiredColumns, Notes);
                //Return the file as byte array
                return pck.GetAsByteArray();
            }
        }

        /// <summary>
        /// Creates an Excel file for a given template with more than one sheet and fills it with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <param name="printHeaders">print the headers in the first row and merge the header groups</param>
        /// <param name="printColumnNames">print the column names above the data</param>
        /// <param name="addFiltersOnColumnNames">add filters for the columns on the column names</param>
        /// <param name="applyFormatting">apply the template formatting to the headers and column names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        /// <returns>Excel file as byte array</returns>
        public byte[] DownloadExcelFromList<T>(List<ExcelSheetListOfObjects<T>> sheetSetup, string templatePath, string language, bool printHeaders = true, bool printColumnNames = true, bool addFiltersOnColumnNames = true, bool applyFormatting = true, bool autoFitColumns = true, string Notes = null)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelSheetTemplate excelSheetTemplate = new ExcelSheetTemplate(templatePath, language);

                for (int i = 0; i < excelSheetTemplate.sheets.Count; i++)
                {
                    excelSheetTemplate.sheets[i].Notes = Notes;
                    FillExcelSheetWithData(pck, excelSheetTemplate.sheets[i], sheetSetup[i].ListOfObjects, language, printHeaders, printColumnNames, addFiltersOnColumnNames, applyFormatting, autoFitColumns);
                }
                //Return the file as byte array
                return pck.GetAsByteArray();
            }
        }

        /// <summary>
        /// Creates an Excel file for a given SheetList and fills it with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="SheetList"></param>
        /// <returns>Excel file as byte array</returns>
        public byte[] DownloadExcelFromSheetList<T>(List<ExcelSheetSetup<T>> SheetList)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (var sheet in SheetList)
                    FillExcelSheetWithData(pck, sheet.listOfObjects, sheet.templatePath, sheet.language, sheet.sheetName, sheet.printHeaders, sheet.printColumnNames, sheet.addFiltersOnColumnNames, sheet.applyFormatting, sheet.autoFitColumns, sheet.printFooters, false, sheet.Notes);

                //Return the file as byte array
                return pck.GetAsByteArray();
            }
        }

        /// <summary>
        /// Fills given Excel document for a given template with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="pck"></param>ExcelPackage that needs to be filled with data
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <param name="printHeaders">print the headers in the first row and merge the header groups</param>
        /// <param name="printColumnNames">print the column names above the data</param>
        /// <param name="addFiltersOnColumnNames">add filters for the columns on the column names</param>
        /// <param name="applyFormatting">apply the template formatting to the headers and column names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        private void FillExcelSheetWithData<T>(ExcelPackage pck, List<T> listOfObjects, string templatePath, string language, string sheetName, bool printHeaders = true, bool printColumnNames = true, bool addFiltersOnColumnNames = true, bool applyFormatting = true, bool autoFitColumns = true, bool printFooters = false, bool addAsteriskOnRequiredColumns = false, string Notes = null)
        {
            //Keep track of the current row
            int currentRow = 1;

            //Get the template
            ExcelTemplate excelTemplate = new ExcelTemplate(templatePath, language, Notes);

            //Create the worksheet
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

            FillExcelTemplateWithData(excelTemplate, ws, listOfObjects, printHeaders, currentRow, printColumnNames, addFiltersOnColumnNames, autoFitColumns, printFooters, addAsteriskOnRequiredColumns);
        }

        /// <summary>
        /// Fills given Excel document for a given template with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="pck"></param>ExcelPackage that needs to be filled with data
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="templatePath">the path to the template</param>
        /// <param name="sheetName">the name of the Excel sheet</param>
        /// <param name="language">the language for the template (e.g. "en", "da"...)</param>
        /// <param name="printHeaders">print the headers in the first row and merge the header groups</param>
        /// <param name="printColumnNames">print the column names above the data</param>
        /// <param name="addFiltersOnColumnNames">add filters for the columns on the column names</param>
        /// <param name="applyFormatting">apply the template formatting to the headers and column names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        private void FillExcelSheetWithData<T>(ExcelPackage pck, ExcelTemplate excelTemplate, List<T> listOfObjects, string language, bool printHeaders = true, bool printColumnNames = true, bool addFiltersOnColumnNames = true, bool applyFormatting = true, bool autoFitColumns = true, bool printFooters = false, bool addAsteriskOnRequiredColumns = false)
        {
            //Keep track of the current row
            int currentRow = 1;

            //Create the worksheet
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add(excelTemplate.Name);

            FillExcelTemplateWithData(excelTemplate, ws, listOfObjects, printHeaders, currentRow, printColumnNames, addFiltersOnColumnNames, autoFitColumns, printFooters, addAsteriskOnRequiredColumns);
        }

        /// <summary>
        /// Loads data from a collection in a given excel worksheet by using a list of members
        /// </summary>
        /// <typeparam name="T">the type of the objects in the collection</typeparam>
        /// <param name="Collection">the list of objects to be written in the Excel</param>
        /// <param name="ws">the worksheet</param>
        /// <param name="startRow">1 based starting row index<</param>
        /// <param name="startColumn">1 based starting column index<</param>
        /// <param name="Members">a list of members to be filled</param>
        private void LoadExcelFromCollection<T>(IEnumerable<T> Collection, ExcelWorksheet ws, int startRow, int startColumn, MemberInfo[] Members)
        {
            var type = typeof(T);
            int row = startRow;
            int col = startColumn;
            foreach (var item in Collection)
            {
                col = startColumn;
                foreach (var t in Members)
                {
                    if (t is PropertyInfo)
                    {
                        ws.Cells[row, col++].Value = ((PropertyInfo)t).GetValue(item, null);
                    }
                    else if (t is FieldInfo)
                    {
                        ws.Cells[row, col++].Value = ((FieldInfo)t).GetValue(item);
                    }
                    else if (t is MethodInfo)
                    {
                        ws.Cells[row, col++].Value = ((MethodInfo)t).Invoke(item, null);
                    }
                }
                row++;
            }
        }

        /// <summary>
        /// Loads data from a collection in a given excel worksheet by using a list of column templates
        /// </summary>
        /// <typeparam name="T">the type of the objects in the collection</typeparam>
        /// <param name="Collection">the list of objects to be written in the Excel</param>
        /// <param name="ws">the worksheet</param>
        /// <param name="startRow">1 based starting row index</param>
        /// <param name="columns">a list of column templates to be filled</param>
        private void LoadExcelFromCollection<T>(IEnumerable<T> Collection, ExcelWorksheet ws, int startRow, List<ColumnTemplate> columns)
        {
            foreach (var c in columns)
            {
                int row = startRow;
                foreach (var item in Collection)
                {
                    var type = item.GetType();
                    if (type != null)
                    {
                        var property = type.GetProperty(c.databaseColumn);
                        if (property != null)
                        {
                            var value = property.GetValue(item, null);
                            if (value != null)
                            {
                                ws.Cells[row, columns.IndexOf(c) + 1].Value = value;
                                if (c.type == "Date")
                                {
                                    DateTime minDate = DateTime.MinValue;
                                    if (DateTime.TryParse(ws.Cells[row, columns.IndexOf(c) + 1].Value.ToString(), out minDate))
                                    {
                                        ws.Cells[row, columns.IndexOf(c) + 1].Value = minDate.ToShortDateString();
                                    }
                                    ws.Cells[row, columns.IndexOf(c) + 1].Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                                }
                                else if (c.type == "DateTime")
                                {
                                    ws.Cells[row, columns.IndexOf(c) + 1].Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.LongDatePattern;
                                }
                                else if (c.type == "Boolean")
                                {
                                    ws.Cells[row, columns.IndexOf(c) + 1].Value = (bool)value ? TrueString : FalseString;
                                }
                                else
                                {
                                    ws.Cells[row, columns.IndexOf(c) + 1].Style.Numberformat.Format = "@";
                                }
                            }
                        }

                        else
                        {
                            var method = type.GetMethod(c.databaseColumn);
                            if (method != null)
                            {
                                var value = method.Invoke(item, null);
                                if (value != null)
                                {
                                    ws.Cells[row, columns.IndexOf(c) + 1].Value = value;
                                }
                            }
                            else
                            {
                                var field = type.GetField(c.databaseColumn);
                                if (field != null)
                                {
                                    var value = field.GetValue(item);
                                    if (value != null)
                                    {
                                        ws.Cells[row, columns.IndexOf(c) + 1].Value = value;
                                    }
                                }
                            }
                        }
                    }
                    row++;
                }
                if (c.formatting != null)
                {
                    c.formatting.ApplyFormattingToCells(ws.Cells[startRow, columns.IndexOf(c) + 1, row - 1, columns.IndexOf(c) + 1]);
                }
            }
        }

        /// <summary>
        /// Filter the members using the list of columns from the template
        /// </summary>
        /// <param name="objMemberInfo">The member to be filtered</param>
        /// <param name="filter">A list of column templates that contains the names of all the columns</param>
        /// <returns>true if the member is in the template</returns>
        private static bool DelegateToSearchCriteria(MemberInfo objMemberInfo, Object filter)
        {
            List<ColumnTemplate> ct = (List<ColumnTemplate>)filter;
            foreach (ColumnTemplate item in ct)
            {
                if (objMemberInfo.Name.ToString() == item.databaseColumn)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Fills given excel template with data from a given list of objects
        /// </summary>
        /// <typeparam name="T">the type of the objects</typeparam>
        /// <param name="excelTemplate">Excel template that needs to be filled with data</param>
        /// <param name="ws">Excel worksheet </param>
        /// <param name="listOfObjects">the list of objects to be written in the Excel</param>
        /// <param name="printHeaders">print the headers in the first row and merge the header groups</param>
        /// <param name="currentRow"></param>
        /// <param name="printColumnNames">print the column names above the data</param>
        /// <param name="addFiltersOnColumnNames">add filters for the columns on the column names</param>
        /// <param name="autoFitColumns">set the column width to automatically wrap the data</param>
        private void FillExcelTemplateWithData<T>(ExcelTemplate excelTemplate, ExcelWorksheet ws, List<T> listOfObjects, bool printHeaders, int currentRow, bool printColumnNames, bool addFiltersOnColumnNames, bool autoFitColumns, bool printFooters = false, bool addAsteriskOnRequiredColumns = false)
        {
            if (printHeaders && excelTemplate.headerMappings != null && excelTemplate.headerMappings.Count > 0)
            {
                int j = 1;
                //Add headers
                for (int i = 0; i < excelTemplate.headerMappings.Count(); i++)
                {
                    //Add the header text
                    ws.Cells[currentRow, j].Value = excelTemplate.headerMappings[i].headerText;

                    //Get the number of columns in this group
                    int numMergedColls = excelTemplate.columnMappings.Where(cm => cm.mergeGroup == excelTemplate.headerMappings[i].mergeGroup).Count();
                    if (numMergedColls == 0)
                        numMergedColls = 1;

                    //Merge the header for the columns in the group
                    if (numMergedColls > 1)
                    {
                        ws.Cells[currentRow, j, currentRow, j + numMergedColls - 1].Merge = true;
                        ws.Cells[currentRow, j, currentRow, j + numMergedColls - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    //Apply the formatting if there is one
                    if (excelTemplate.headerMappings[i].formatting != null)
                    {
                        if (numMergedColls > 1)
                        {
                            excelTemplate.headerMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRow, j, currentRow, j + numMergedColls - 1]);
                        }
                        else
                        {
                            excelTemplate.headerMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRow, j]);
                        }
                    }
                    j += numMergedColls;
                }
                currentRow++;
            }

            if (printColumnNames)
            {
                //Add column names
                for (int i = 0; i < excelTemplate.columnMappings.Count(); i++)
                {
                    //Add the column text
                    if (addAsteriskOnRequiredColumns == true && excelTemplate.columnMappings[i].nullable == false)
                    {
                        ws.Cells[currentRow, i + 1].Value = excelTemplate.columnMappings[i].excelColumn + " *";
                    }
                    else
                    {
                        ws.Cells[currentRow, i + 1].Value = excelTemplate.columnMappings[i].excelColumn;
                    }
                    //Apply the formatting if there is one
                    if (excelTemplate.columnMappings[i].columnheaderformating != null)
                    {
                        excelTemplate.columnMappings[i].columnheaderformating.ApplyFormattingToCells(ws.Cells[currentRow, i + 1]);
                    }
                    else if (excelTemplate.columnMappings[i].formatting != null)
                    {
                        excelTemplate.columnMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRow, i + 1]);
                    }
                }

                if (addFiltersOnColumnNames)
                {
                    //Add filters for the column headers
                    ws.Cells[currentRow, 1, currentRow, excelTemplate.columnMappings.Count()].AutoFilter = true;
                }
                currentRow++;
            }



            //Populate the data
            if (listOfObjects.Count() > 0)
            {
                //Type objType = typeof(T);
                //MemberInfo[] arrayMemberInfo = objType.FindMembers(MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, new MemberFilter(DelegateToSearchCriteria), excelTemplate.columnMappings);
                //ws.Cells["A3"].LoadFromCollection<T>(listOfObjects, false, OfficeOpenXml.Table.TableStyles.None, BindingFlags.Public | BindingFlags.Instance, arrayMemberInfo);
                LoadExcelFromCollection<T>(listOfObjects, ws, currentRow, excelTemplate.columnMappings);

                currentRow += listOfObjects.Count;
                //set auto column width
                if (autoFitColumns)
                {
                    ws.Cells.AutoFitColumns();
                }

                if (excelTemplate.columnMappings.Any(x => x.formatting != null && x.formatting.width.HasValue))
                {
                    List<ColumnTemplate> columnsWithFixedWidth = excelTemplate.columnMappings.Where(x => x.formatting != null && x.formatting.width.HasValue).ToList();
                    foreach (var column in columnsWithFixedWidth)
                    {
                        double fixedWidth = (column.formatting.width.Value / 7d) + 1;
                        int columnNumber = excelTemplate.columnMappings.IndexOf(column);
                        ws.Column(columnNumber + 1).Width = fixedWidth;
                    }
                }
            }


            if (printFooters)
            {
                //Add footers
                for (int i = 0; i < excelTemplate.footerMappings.Count(); i++)
                {
                    //Apply the formatting if there is one
                    if (excelTemplate.footerMappings[i].formatting != null)
                    {
                        excelTemplate.footerMappings[i].formatting.ApplyFormattingToCells(ws.Cells[currentRow, 1, currentRow, excelTemplate.columnMappings.Count()]);
                    }
                }
                currentRow++;
            }

            //ADD NOTES
            if (!string.IsNullOrEmpty(excelTemplate.Notes))
            {
                //merge for all columns
                int numMergedColls = excelTemplate.columnMappings.Count;
                ws.Cells[currentRow, 1, currentRow, numMergedColls].Merge = true;
                ws.Cells[currentRow, 1, currentRow, numMergedColls].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                ws.Cells[currentRow, 1].Value = excelTemplate.Notes;

                currentRow++;
            }

        }
    }
}
